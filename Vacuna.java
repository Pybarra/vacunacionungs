package tpVacunacionUngs;

public abstract class Vacuna {
	/*
	 *  STRING NOMBRE 
	 *  INT TEMP ---> SOLAMENTE DOS VALORES 3 o -18
	 *  PERSONA PERsONA
	 */  
	
	/*
	 *  VACUNA 1 y VACUNA 2 HEREDAN DE VACUNA
	 */
	//SEGUN QUE EL GRUPO QUE PERTENEZCA SE CONFIRMA QUE TIPO DE VACUNA PUEDE RECIBIR
	boolean aplicable(Persona per) {
		return false;
	}
	// HAY DE ESTA VACUNA?
	abstract boolean vacunaNoDisp(); //METODO ABSTRACTO
	//UNA VEZ QUE PASA EL DIA SE VACIA LA LISTA DE VACUNAS DISPONIBLES  Y SE VUELEN A LLENAR
	abstract void vaciarConj();
	
	
}
